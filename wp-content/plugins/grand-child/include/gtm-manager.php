<?php

// Add scripts to wp_head()
function gtm_head_script() {
	 $id =  get_option( 'gtm_script_insert');
	if (!empty($id)) { ?>
	 
	 	<!-- Google Tag Manager -->
	<?php echo "<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
		new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
		j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
		'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
		})(window,document,'script','dataLayer', '$id' );</script> ";
		?> 
		<!-- End Google Tag Manager -->

		<!-- TrafficTracking source  -->
	<?php echo "<script src='/wp-content/plugins/grand-child/js/trafficSourceTracker.js'></script><script>//initiate script
trafficSrcCookie.setCookie();</script> ";
		?> 
		<!-- End TrafficTracking source  -->

	 <?php } ?>
<?php }
 

/*
* This solution makes two assumptions:
* The theme’s opening <body> tag ends with <?php body_class(); ?>.
*    For most scenarios this should be the case
* There isn’t any other plugin which filters the body_class hook and has a lower priority (the default priority is 10 – this is set to 10000).
*/

add_filter( 'body_class', 'gtm_body_script', 10000 );
function gtm_body_script( $classes ) {
$id = get_option( 'gtm_script_insert');

	if (!empty($id)) {
		$block = <<<BLOCK
 
<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=$id"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager -->
 
BLOCK;
 
	$classes[] = '">' . $block . '<br style="display:none';      
	return $classes;
	}
}

?>