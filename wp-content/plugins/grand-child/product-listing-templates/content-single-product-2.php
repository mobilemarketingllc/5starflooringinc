<?php
global $post;
$flooringtype = $post->post_type;
$brand = get_field('brand') ;
$sku = get_field('sku') ;
$image = get_field('swatch_image_link') ? get_field('swatch_image_link'):"http://placehold.it/168x123?text=No+Image";
    
    if (strpos($image, 's7.shawimg.com') !== false) {
        if (strpos($image, 'http') === false) {
            $image = "http://" . $image;
        }
        $image = "https://mobilem.liquifire.com/mobilem?source=url[".$image . "]&scale=size[300x300]&sink";
    } else {
        if (strpos($image, 'http') === false) {
            $image = "https://" . $image;
        }
        $image= "https://mobilem.liquifire.com/mobilem?source=url[".$image . "]&scale=size[600x400]&sink";
    }
?>
</div></div></div>
<style type="text/css">
.breadcrumbs{ display:none;}
</style>
	<?php
        // check if the repeater field has rows of data
        if (get_field('gallery_room_images')) {
            // loop through the rows of data
            $gallery_images = get_field('gallery_room_images');

            $gallery_img = explode("|", $gallery_images);
             
            // loop through the rows of data
            foreach ($gallery_img as  $key=>$value) {
                $room_image = $value;

            
                if (strpos($room_image, 's7.shawimg.com') !== false) {
                    if (strpos($room_image, 'http') === false) {
                        $room_image = "http://" . $room_image;
                    }
                    $room_image = $room_image ;
                } else {
                    if (strpos($room_image, 'http') === false) {
                        $room_image = "https://" . $room_image;
                    }
                    $room_image= "https://mobilem.liquifire.com/mobilem?source=url[".$room_image . "]&scale=size[1600x1000]&sink";
                } ?>
	<?php
            break;
            }
        }
    ?>
	<div class="product-banner-img" <?php if ($room_image) {
        ?>style="background-image:url('<?php echo $room_image; ?>');"<?php
    } ?>></div>
	<div class="container">
		<div class="row">
			<div class="fl-content product col-sm-12">
				<div class="product-detail-layout-2">
					<article <?php post_class('fl-post'); ?> id="fl-post-<?php the_ID(); ?>">
						<div class="fl-post-content clearfix grey-back" itemprop="text">
							<div class="row">
								<div class="col-md-3 col-sm-12 product-box pdp2-leftbox">
									<?php
                                        $dir = WP_PLUGIN_DIR.'/grand-child/product-listing-templates/includes/product-brand-logos.php';
                                        include_once($dir);
                                    ?>
									<?php if (get_field('parent_collection')) {
                                        ?>
										<h4><?php the_field('parent_collection'); ?> </h4>
									<?php
                                    } ?>
								
									<?php if (get_field('collection')) {
                                        ?>
										<h2 class="fl-post-title" itemprop="name"><?php the_field('collection'); ?></h2>
									<?php
                                    } ?>

									<?php if (get_field('color')) {
                                        ?>
										<h1 class="fl-post-title" itemprop="name"><?php the_field('color'); ?></h1>
									<?php
                                    } ?>
									<?php /*?><h5>Style No. <?php the_field('style'); ?>, Color No. <?php the_field('color_no'); ?></h5> <?php*/ ?>
									
									<div class="product-colors">
										<?php
                                            $familysku = get_post_meta($post->ID, 'collection', true);

                                            $args = array(
                                                'post_type'      => $flooringtype,
                                                'posts_per_page' => -1,
                                                'post_status'    => 'publish',
                                                'meta_query'     => array(
                                                    array(
                                                        'key'     => 'collection',
                                                        'value'   => $familysku,
                                                        'compare' => '='
                                                    ),
                                                    array(
                                                        'key' => 'swatch_image_link',
                                                        'value' => '',
                                                        'compare' => '!='
                                                        )
                                                )
                                            );
                                            $the_query = new WP_Query($args);
                                        ?>
										<ul>
											<li class="color-count" style="font-size:14px;"><?php echo $the_query ->found_posts; ?> Colors Available</li>
										</ul>
									</div>
								</div>

								<div class="col-md-6 col-sm-12 product-swatch">
									<div class="product-swatch-inner">
									<?php
                                        $dir = WP_PLUGIN_DIR.'/grand-child/product-listing-templates/includes/product-images.php';
                                        include($dir);
                                    ?>
									</div>
								</div>
								
								<div class="col-md-3 col-sm-12 product-box">
                                    <div class="button-wrapper">
                                        <a href="/flooring-coupon/?keyword=<?php echo $_COOKIE['keyword']; ?>&brand=<?php echo $brand;?>" class="button alt">Get Coupon</a>
                                        <a href="/contact-us/" class="button">Contact Us</a>
                                        <a href="/flooring-financing/" class="button">Get Financing</a>
                                        <!-- <a href="/schedule-measurement/" class="button">Schedule a Measurement</a> -->
                                    </div>
								</div>
									<div class="clearfix"></div>
							</div>
							<?php
                                $dir = WP_PLUGIN_DIR.'/grand-child/product-listing-templates/includes/product-color-slider.php';
                                include($dir);
                            ?>
								<div class="clearfix"></div>
							<div id="product-attributes-wrap">
								<?php
                                    $dir = WP_PLUGIN_DIR.'/grand-child/product-listing-templates/includes/product-attributes.php';
                                    include($dir);
                                ?>
							</div>
						</div>
					</article>
			</div>
			<?php
			$title = get_the_title();

			$jsonld = array('@context'=>'https://schema.org/','@type'=>'Product','name'=> $title,'image'=>$image,'description'=>$title,'sku'=>$sku,'mpn'=>$sku,'brand'=>array('@type'=>'thing','name'=>$brand), 
			'offers'=>array('@type'=>'offer','priceCurrency'=>'USD','price'=>'00','priceValidUntil'=>''));
			?>
			<?php echo '<script type="application/ld+json">'.json_encode($jsonld).'</script>';	?>