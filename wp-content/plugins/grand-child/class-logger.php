<?php

trait WP_Example_Logger {

	/**
	 * Really long running process
	 *
	 * @return int
	 */
	public function really_long_running_task() {
		return sleep( 1 );
	}

	/**
	 * Log
	 *
	 * @param string $message
	 */
	public function log( $message ) {
		error_log( $message );
	}

	/**
	 * Get lorem
	 *
	 * @param string $name
	 *
	 * @return string
	 */
	protected function get_message( $name,$id ) {

		$msg = $name.'-'.$id.' Inserted Successfully' ;
		
		return $msg;
	}

	/**
	 * Insert Product 
	 *
	 * @param string $message
	 */
	 public function insert_product( $data ,$main_category) {

		if($data['status']=='active' && trim($data['swatch'])!="" ){

			write_log($main_category);

			// args for checking already inserted product
			 // find list of states in DB
			 global $wpdb;	
			 $product_check_table = $wpdb->prefix."product_check";
			$qry = "SELECT * FROM $product_check_table WHERE skuid = '".$data['sku']."'";
			$skus = $wpdb->get_results( $qry );
			$post_id =0;

		 if(count($skus) > 0){
			write_log('Already-');
			
			$post_id = $skus[0]->post_id;
			error_log("Updated POST ID $post_id",$skus[0]->post_id);
			$del = "DELETE FROM {$wpdb->postmeta} WHERE post_id = '".$post_id."'";
			$wpdb->query( $del );

		 }else{			

		// Insert post data.

			$my_post = array(
				'post_title'    => $data['name'].' '.$data['sku'],
				'post_content'  => '',
				'post_type'  => $main_category,
				'post_status'   => 'publish',
				'post_author'   => 1,				
			);

			
			
			// Insert the post into the database.
			$post_id = wp_insert_post( $my_post );
			write_log('New-'.$post_id);
		
			$wpdb->insert( $product_check_table, array('skuid' => $data['sku'], 'post_id' =>$post_id), array( '%s', '%s'));
			
		}
		
		$values="";
    foreach($data as $key => $value){
			$arryColumnNotREquired= array("name","post_type");
				if( !in_array($key, $arryColumnNotREquired)){
					if(isset($value) && $value !=""){
						//write_log("VAl::", $key,$value);
						
						if($key == 'collection_name'){
							$values.= "(".$post_id.",'collection','".wp_slash($value)."')," ;
						}
						if( $key == 'in_stock'){
							$values.= "(".$post_id.",'construction_facet','".wp_slash($value)."')," ;
						}
						if( $key == 'installation'){
							$values.= "(".$post_id.",'installation_method','".wp_slash($value)."')," ;
						}
						if( $key == 'warranty_text'){
							$values.= "(".$post_id.",'warranty_info','".wp_slash($value)."')," ;
						}
						if( $key == 'swatch'){
							$values.= "(".$post_id.",'swatch_image_link','".wp_slash($value)."')," ;
						}
						if( $key == 'gallery_images'){
							$values.= "(".$post_id.",'gallery_room_images','".wp_slash($value)."')," ;
						}
						else{
							$values.= "(".$post_id.",'".$key."','".wp_slash($value)."')," ;
						}

							
					}
				}
			
		} 

	
	 return $values;
	 return $post_id;
		
	}
	 }

}